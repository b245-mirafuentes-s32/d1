// require http that contains the node js module
const http = require('http');
let port = 4000

const server = http.createServer((request, response) =>{
	if(request.url == '/items' && request.method == 'GET'){
		response.writeHead(200, {'Content-type': 'text/plain'})
		response.end("Data retrieve from /items")
	}
	if(request.url == '/items' && request.method == 'POST'){
		response.writeHead(200, {'Content-type': 'text/plain'})
		response.end('Data to be sent to the database')
	}
})

server.listen(port);
console.log(`Server is running at port ${port}!`)