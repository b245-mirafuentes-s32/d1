const http = require('http')

// Mock Database
let directory = [
		{
			"name": "Brandon",
			"email": "brandon@gmail.com"
		},
		{
			"name": "Jobert",
			"email": "jobert@gmail.com"
		}
	]

const port = 3000

const server = http.createServer((request, response) => {
	// Add a route here for GET method that will retrieved the content of our database
	if(request.url == '/users' && request.method == 'GET'){

		// sets the status to 200
		// application/json it sets the response to JSON Data Type
		response.writeHead(200, {'Content-type': 'application/json'})

		// Input has to be data type STRING hence the JSON.stringify(method)
		response.write(JSON.stringify(directory))

		// to end the route we add response.end
		response.end()
	}else if(request.url == '/users' && request.method == 'POST'){
		let requestBody = ""

		// to capture the input of the user and store the variable requestBody
		request.on('data', function(data){
			requestBody += data
		})

		// to end the request and set the function that will be invoked right before it ends.
		request.on('end', function(){
			console.log(typeof requestBody)
			console.log(requestBody)

			requestBody = JSON.parse(requestBody)
			console.log(typeof requestBody)

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}
			console.log(newUser)

			directory.push(newUser)

			response.writeHead(200, {'Content-type': 'application/json'})
			response.write(JSON.stringify(newUser))
			response.end()

		})
	}
})

server.listen(port)
console.log(`Server is running at port ${port}`)